FROM maven:3.6.3-openjdk-8

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN apt install nodejs

RUN nodejs --version && npm --version


LABEL usage="Maven(3.6.3) + Node(12.18.4)"

LABEL version="Maven(3.6.3)"
LABEL updated="09/22/2020"

